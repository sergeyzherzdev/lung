#!/usr/bin/env bash

pip3 install mmcv-full
git clone https://github.com/open-mmlab/mmdetection.git
cd mmdetection
pip3 install -e .
cd ..

