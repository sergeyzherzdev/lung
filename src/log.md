# Research Log

---
21.04.18

## ResNeXt50 based model
The ResNeXt-50 backbone based model was trained overnight using the Group Normalization instead of BN 
from the pretrained backbone. 

Resulting mAP @ IoU=0.4 is 0.164 for VinBig validation and 0.092 for 880 original dataset images.
It looks underfitted comparing with the original VGG backboned model, so more 
training data and/or longer training schedule should be used to feed more complicated models.

---
21.04.17

## Results update
Using the Pascal VOC defined mAP @ IoU>0.4 according to the VinBig rules, the baseline SSD512 
result is 0.256 (at the bronze-silver edge of the leaderboard), which looks reasonable.
```bash
python3 mmdetection/tools/test.py ssd_14.py work_dirs/ssd_14/epoch_24.pth --eval bbox --eval-options iou_thrs=[0.4]
```

## Fine-tuning
Real data preparation script (`real_data.py`), creates 830/50 images train/val split plus 
the 880 images sized whole validation set

Using the same trained model (configuration script `ssd_14_8.py`) got the 0.101 mAP @ IoU=4 
for the 880 images and 0.169 mAP for 50 validation images real data with some assumptions:
- `Mass` and `Nodule` classes were joined similar to the VinBig labels
- The `Pneumonia` class was assigned similar to the `Lung Opacity`

Fine-tuning for 8 epochs with reduced LR for 830/50 split (`ssd_8.py`) 
shows the 0.276 mAP @ IoU=4 (validation), 
which outperforms the 0.169 for original 14-classes model with a margin.

---
21.04.12

## Baseline results
The baseline SSD512 model achieves 0.093 validation mAP for IoU 0.5:0.95 for the VinBig dataset
which is quite poor.
So the plan is:
- Modify the default validation range to 0.4:0.95 to match with the leaderboard
- Finetune for the main 8-classes dataset
- Experiment with backbone (replace the VGG with ResNext)
- Try other more complicated models (Cascade RCNN and CornerNet)

## Dataset adaptation
Since the VinBig dataset doesn't contain the `Pneumonia` class, I've explored a little about it.
According to the [article](https://www.kaggle.com/zahaviguy/what-are-lung-opacities) it looks like the VinBig
`Lung Opacity` class corresponds the `Pneumonia` quite well (non-pneumonia opacities like mass or 
effusion are labeled at VinBig as different ones explicitly). 
So it looks as if the VinBig pretrained model should generalize well for the main dataset, 
the only concern is about `Mass` and `Nodule` classes which are joined at the VinBig.

---
21.04.11

Data conversion and model config debug.
The baseline model training was started (24 epochs take ~5 hours for 2 GPUs)
```
python3 mmdetection/tools/train.py ssd_14.py
```
OR for multi-GPU training
```
python3 -m torch.distributed.launch --nproc_per_node=2 mmdetection/tools/train.py ssd_14.py --gpus 2 --launcher pytorch
```

---
21.04.10

## Baseline training
Original environment preparation code:
- Dependencies install (see the `install.sh`)
- VinBig data preparation (`vinbig_data.py`), 14K/1K images train/val split
- The model configuration script (`ssd_14.py`)

---
21.04.09

## The model
While the current Transformer-based SotA models like [Swin](https://arxiv.org/pdf/2103.14030v1.pdf) 
or [DPT](https://arxiv.org/pdf/2103.13413.pdf) look promising outperforming the R-CNN like models, 
I've decided to start with simple SSD based one due to the following:
- Well examined open implementations are available, including the pyTorch oriented 
[MMDetection toolbox](https://github.com/open-mmlab/mmdetection). It's moduled well so it 
will be easier to experiment on the model reconfiguration later.
- SSD is quite straight-forward model which is easy to debug and hyperparameter fine-tune to 
apply it to the unusual application domain.
- SSD in general is less resource consuming and can be trained faster comparing to the R-CNN based ones.
- SSD results can be used as first-step baseline for future experiments.

---
21.04.01

## The goal
The ultimate goal of the project is Deep Learning based object detection system for lungs XRay's.
Different types of issues should be detected.

## Data
The main dataset is placed [here](https://nihcc.app.box.com/v/ChestXray-NIHCC/folder/36938765345)

It contains *~112K* images but only *880* of them are labeled for 8 object classes:
- `Atelectasis`
- `Cardiomegaly`
- `Infiltrate`
- `Mass`
- `Nodule`
- `Effusion`
- `Pneumothorax`
- `Pneumonia`

It's labeled subset is obviously insufficient for training the model from scratch, 
so any additional data is needed for the pretraining stage.

All other data can be used for unsupervised learning in future (if any).

## VinBig challenge
Similar looking Kaggle [VinBigData Chest X-ray Abnormalities 
Detection challenge](https://www.kaggle.com/c/vinbigdata-chest-xray-abnormalities-detection) 
can be used as additional data, baseline results and inspiration source.

The problem defined almost the same way with little difference:
- 14 object classes are labeled plus explicit `No finding` one
- All 8 original problem classes are presented excluding the `Pneumonia`
- `Mass` and `Nodule` classes are joined as class 8

The dataset contains about 15K labeled images.

Since the original data volume is about 200Gb, the reduced to 500x500 and JPEG compressed 
[version](https://www.kaggle.com/navjotbansal/vinbig-xray-compressed-images-for-yolo-training)
of images will be used.

## Metrics
Since there is no particular target metric is defined, so I suppose to use the 
PASCAL VOC 2010 mean Average Precision (mAP) at *IoU > 0.4* similar to the VinBig challenge rules.

The VinBig leaderboard sets the following baselines:
- 0.365 for the best solution
- 0.3 for Top-10 (gold)
- 0.25 for Top-100 (bronze)