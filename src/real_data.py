import argparse
import os
import csv
import random
import mmcv


# Real dataset classes to the VinBig class_id mapping
classes = {'Atelectasis': 1, 'Cardiomegaly': 3, 'Infiltrate': 6, 'Pneumonia': 7, 'Nodule': 8, 'Mass': 8,
           'Effusion': 10, 'Pneumothorax': 12}


def make_json(ids, labels, categories, images_path, file_name):
    annotations = []
    images = []
    count = 0

    for id, img in enumerate(mmcv.track_iter_progress(ids)):
        img_name = img
        img_path = os.path.join(images_path, img_name)
        height, width = mmcv.imread(img_path).shape[:2]

        images.append(dict(
            id=id,
            file_name=img_name,
            height=height,
            width=width))

        for label in labels[img]:
            # The CSV columns are labeled quite weird
            x_min, y_min, w, h = float(label['Bbox [x']), float(label['y']), float(label['w']), float(label['h]'])
            assert (x_min + w <= width and y_min + h <= height)
            data_anno = dict(
                image_id=id,
                id=count,
                category_id=classes[label['Finding Label']],
                bbox=[x_min, y_min, w, h],
                area=w * h,
                iscrowd=0)
            annotations.append(data_anno)
            count += 1

    coco_format_json = dict(
        images=images,
        annotations=annotations,
        categories=categories)
    mmcv.dump(coco_format_json, file_name)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Convert the real dataset to COCO format')
    parser.add_argument('data', help='The dataset path')
    parser.add_argument('-o', '--out-dir', help='output path')
    parser.add_argument('--nval', default=50, type=int, help='validation subset size')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    data_path = args.data
    images_path = os.path.join(data_path, 'images')
    labels_csv = os.path.join(data_path, 'BBox_List_2017.csv')
    out_dir = args.out_dir if args.out_dir else data_path
    mmcv.mkdir_or_exist(out_dir)

    labels = {}

    vb_class_names = ['Aortic enlargement', 'Atelectasis', 'Calcification', 'Cardiomegaly', 'Consolidation', 'ILD',
                   'Infiltration', 'Lung Opacity', 'Nodule/Mass', 'Other lesion', 'Pleural effusion',
                   'Pleural thickening', 'Pneumothorax', 'Pulmonary fibrosis']
    categories = [{'id': i, 'name': n} for i, n in enumerate(vb_class_names)]  # 14 is for No finding empty class
    # TODO: fill the class names by the labels data

    with open(labels_csv) as f:
        reader = csv.DictReader(f)
        for row in reader:
            fname = row['Image Index']
            if fname not in labels:
                labels[fname] = []  # list of objects
            labels[fname].append(row)

    # TODO: Reduce the categories and filter the list of images according to reduced classes set

    ids = list(labels.keys())
    print(len(ids), 'images are labeled')

    # Filter by file exist
    ids = list(filter(lambda id: os.path.isfile(os.path.join(images_path, id)), ids))
    print(len(ids), 'images are exist')

    make_json(ids, labels, categories, images_path, os.path.join(out_dir, 'real.json'))
    # Build the train / val split
    random.shuffle(ids)
    make_json(ids[:args.nval], labels, categories, images_path, os.path.join(out_dir, 'real_val.json'))
    make_json(ids[args.nval:], labels, categories, images_path, os.path.join(out_dir, 'real_train.json'))


if __name__ == '__main__':
    main()
