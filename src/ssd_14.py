# The new config inherits a base config to highlight the necessary modification
_base_ = 'mmdetection/configs/ssd/ssd512_coco.py'

# We also need to change the num_classes in head to match the dataset's annotation
model = dict(
    bbox_head=dict(num_classes=14),
    )

# Modify dataset related settings
dataset_type = 'CocoDataset'
data_root = '../VinBig/'
classes = ('Aortic enlargement', 'Atelectasis', 'Calcification', 'Cardiomegaly', 'Consolidation', 'ILD', 'Infiltration',
           'Lung Opacity', 'Nodule/Mass', 'Other lesion', 'Pleural effusion', 'Pleural thickening', 'Pneumothorax',
           'Pulmonary fibrosis')
data = dict(
    samples_per_gpu=8,
    workers_per_gpu=3,
    train=dict(
        type='RepeatDataset',
        times=5,
        dataset=dict(
            type=dataset_type,
            ann_file='train.json',
            classes=classes,
            img_prefix=data_root + 'train/',
        )),
    val=dict(
        type=dataset_type,
        ann_file='val.json',
        classes=classes,
        img_prefix=data_root + 'train/',
        ),
    test=dict(
        type=dataset_type,
        ann_file='val.json',
        classes=classes,
        img_prefix=data_root + 'train/',
        ),
    )
