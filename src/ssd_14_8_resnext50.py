# The new config inherits a base config to highlight the necessary modification
_base_ = 'mmdetection/configs/ssd/ssd512_coco.py'

# We also need to change the num_classes in head to match the dataset's annotation
conv_cfg = dict(type='ConvWS')
norm_cfg = dict(type='GN', num_groups=32, requires_grad=True)
model = dict(
    pretrained='open-mmlab://jhu/resnext50_32x4d_gn_ws',
    backbone=dict(
        _delete_=True,
        type='ResNeXt',
        depth=50,
        groups=32,
        base_width=4,
        num_stages=4,
        out_indices=(0, 1, 2, 3),
        frozen_stages=1,
        style='pytorch',
        conv_cfg=conv_cfg,
        norm_cfg=norm_cfg
        ),
    bbox_head=dict(
        num_classes=14,
        in_channels=(256, 512, 1024, 2048),
        anchor_generator=dict(
            strides=[4, 8, 16, 32],
            ratios=[[2], [2, 3], [2, 3], [2, 3]]
            ),
        ),
    )

# Modify dataset related settings
dataset_type = 'CocoDataset'
data_root = '../data/'
classes = ('Aortic enlargement', 'Atelectasis', 'Calcification', 'Cardiomegaly', 'Consolidation', 'ILD', 'Infiltration',
           'Lung Opacity', 'Nodule/Mass', 'Other lesion', 'Pleural effusion', 'Pleural thickening', 'Pneumothorax',
           'Pulmonary fibrosis')
data = dict(
    samples_per_gpu=8,
    workers_per_gpu=3,
    train=dict(
        type='RepeatDataset',
        times=5,
        dataset=dict(
            type=dataset_type,
            ann_file='real.json',
            classes=classes,
            img_prefix=data_root + 'images/',
        )),
    val=dict(
        type=dataset_type,
        ann_file='real.json',
        classes=classes,
        img_prefix=data_root + 'images/',
        ),
    test=dict(
        type=dataset_type,
        ann_file='real.json',
        classes=classes,
        img_prefix=data_root + 'images/',
        ),
    )
