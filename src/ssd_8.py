# The new config inherits a base config to highlight the necessary modification
_base_ = 'mmdetection/configs/ssd/ssd512_coco.py'

# We also need to change the num_classes in head to match the dataset's annotation
model = dict(
    bbox_head=dict(num_classes=7),
    )

load_from = 'work_dirs/ssd_14/latest.pth'
# lr is set for a batch size of 8
optimizer = dict(type='SGD', lr=0.001, momentum=0.9, weight_decay=0.0001)
optimizer_config = dict(grad_clip=None)
# learning policy
lr_config = dict(
    policy='step',
    warmup='linear',
    warmup_iters=500,
    warmup_ratio=0.001,
    # [7] yields higher performance than [6]
    step=[7])
runner = dict(type='EpochBasedRunner', max_epochs=8)

# Modify dataset related settings
dataset_type = 'CocoDataset'
data_root = '../data/'
classes = ('Atelectasis', 'Cardiomegaly', 'Infiltration', 'Lung Opacity', 'Nodule/Mass', 'Pleural effusion', 'Pneumothorax')
data = dict(
    samples_per_gpu=8,
    workers_per_gpu=3,
    train=dict(
        type='RepeatDataset',
        times=5,
        dataset=dict(
            type=dataset_type,
            ann_file='real_train.json',
            classes=classes,
            img_prefix=data_root + 'images/',
        )),
    val=dict(
        type=dataset_type,
        ann_file='real_val.json',
        classes=classes,
        img_prefix=data_root + 'images/',
        ),
    test=dict(
        type=dataset_type,
        ann_file='real_val.json',
        classes=classes,
        img_prefix=data_root + 'images/',
        ),
    )
