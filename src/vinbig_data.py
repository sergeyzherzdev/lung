import argparse
import os
import csv
import random
import mmcv


def make_json(ids, labels, categories, images_path, file_name):
    annotations = []
    images = []
    count = 0

    for id, img in enumerate(mmcv.track_iter_progress(ids)):
        img_name = img + '.jpg'
        img_path = os.path.join(images_path, img_name)
        height, width = mmcv.imread(img_path).shape[:2]

        images.append(dict(
            id=id,
            file_name=img_name,
            height=height,
            width=width))

        for bb in labels[img]:
            x_min, y_min, x_max, y_max = float(bb['x_min']), float(bb['y_min']), float(bb['x_max']), float(bb['y_max'])
            assert (x_max <= width and y_max <= height)
            data_anno = dict(
                image_id=id,
                id=count,
                category_id=int(bb['class_id']),
                bbox=[x_min, y_min, x_max - x_min, y_max - y_min],
                area=(x_max - x_min) * (y_max - y_min),
                iscrowd=0)
            annotations.append(data_anno)
            count += 1

    coco_format_json = dict(
        images=images,
        annotations=annotations,
        categories=categories)
    mmcv.dump(coco_format_json, file_name)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Convert VinBig dataset to COCO format')
    parser.add_argument('vinbig', help='VinBig data path')
    parser.add_argument('-o', '--out-dir', help='output path')
    parser.add_argument('--nval', default=1000, type=int, help='validation subset size')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    vinbig_path = args.vinbig
    images_path = os.path.join(vinbig_path, 'train')
    labels_csv = os.path.join(vinbig_path, 'train.csv')
    out_dir = args.out_dir if args.out_dir else vinbig_path
    mmcv.mkdir_or_exist(out_dir)

    labels = {}

    class_names = ['Aortic enlargement', 'Atelectasis', 'Calcification', 'Cardiomegaly', 'Consolidation', 'ILD',
                   'Infiltration', 'Lung Opacity', 'Nodule/Mass', 'Other lesion', 'Pleural effusion',
                   'Pleural thickening', 'Pneumothorax', 'Pulmonary fibrosis']
    categories = [{'id': i, 'name': n} for i, n in enumerate(class_names)]  # 14 is for No finding empty class
    # TODO: fill the class names by the labels data

    with open(labels_csv) as f:
        reader = csv.DictReader(f)
        for row in reader:
            if row['image_id'] not in labels:
                labels[row['image_id']] = []  # list of objects
            if int(row['class_id']) < 14:  # ignore No finding labels
                labels[row['image_id']].append(row)

    # TODO: Reduce the categories and filter the list of images according to reduced classes set

    ids = list(labels.keys())
    print(len(ids), 'images are labeled')

    # Filter by file exist
    ids = list(filter(lambda id: os.path.isfile(os.path.join(images_path, id + '.jpg')), ids))
    print(len(ids), 'images are exist')

    # Build the train / val split
    random.shuffle(ids)
    make_json(ids[:args.nval], labels, categories, images_path, os.path.join(out_dir, 'val.json'))
    make_json(ids[args.nval:], labels, categories, images_path, os.path.join(out_dir, 'train.json'))


if __name__ == '__main__':
    main()
